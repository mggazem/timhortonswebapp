package sheridan;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

	@Test
	public void testDrinksRegular() {
		List<String> drinkTypes = MealsService.getAvailableMealTypes(MealType.DRINKS);
		System.out.println("Regular listNotEmpty: " + drinkTypes.isEmpty());
		assertTrue("The following list of drinks is empty", drinkTypes.isEmpty()==false);
	}
	
	@Test
	public void testDrinksException() {
		List<String> drinkTypes = MealsService.getAvailableMealTypes(MealType.DRINKS);
		boolean test = drinkTypes.get(0).equals("No Brand Available");
		System.out.println("Exception drink type is not null: " + drinkTypes.get(0));
		assertTrue("The following drink type is null", test==false);
	}
	@Test
	public void testDrinksBoundaryIn() {
		List<String> drinkTypes = MealsService.getAvailableMealTypes(MealType.DRINKS);
		boolean test = drinkTypes.size()>3;
		System.out.println("boundaryin list is more than 3: " + drinkTypes.size());
		assertTrue("The following list of drinks is contains more than 3 options", test==true);
	}
	@Test
	public void testDrinksBoundaryOut() {
		List<String> drinkTypes = MealsService.getAvailableMealTypes(MealType.DRINKS);
		boolean test = drinkTypes.size()<1;
		System.out.println("boundary out list is less than 1: " + drinkTypes.size());
		assertTrue("The following list of drinks contains less than 1 option", test==false);
	}
}
